puppet-module-designate (25.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Oct 2024 10:36:18 +0200

puppet-module-designate (24.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Apr 2024 14:33:45 +0200

puppet-module-designate (23.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Oct 2023 12:06:18 +0200

puppet-module-designate (22.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Jun 2023 11:14:31 +0200

puppet-module-designate (22.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed runtime depends versions for this release.
  * Rebased fix-api-number-of-threads.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 12 Apr 2023 10:26:46 +0200

puppet-module-designate (21.0.0-2) unstable; urgency=medium

  * Add add_designate_tlds_config.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 16 Feb 2023 12:20:54 +0100

puppet-module-designate (21.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Install the templates folder.

 -- Thomas Goirand <zigo@debian.org>  Thu, 20 Oct 2022 22:16:07 +0200

puppet-module-designate (20.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Removed Switch-to-ini_settings.patch applied upstream.
  * Commit new remainings of above patch: fix-api-number-of-threads.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2022 14:18:34 +0200

puppet-module-designate (20.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 13:46:24 +0100

puppet-module-designate (20.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Fixed Switch-to-ini_settings.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Mar 2022 23:36:30 +0100

puppet-module-designate (19.4.0-3) unstable; urgency=medium

  * Add Switch-to-ini_settings.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 21 Jan 2022 18:09:22 +0100

puppet-module-designate (19.4.0-2) unstable; urgency=medium

  * Clean-up update-alternatives handling.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Jan 2022 09:37:20 +0100

puppet-module-designate (19.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Oct 2021 10:42:31 +0200

puppet-module-designate (19.3.0-1) unstable; urgency=medium

  * Uploading to unstable.
  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 14:24:23 +0200

puppet-module-designate (19.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Sep 2021 13:44:02 +0200

puppet-module-designate (18.4.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 17:11:36 +0200

puppet-module-designate (18.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 06 Apr 2021 12:47:52 +0200

puppet-module-designate (18.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 21:24:20 +0100

puppet-module-designate (18.2.0-1) experimental; urgency=medium

  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.
  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 10:47:00 +0100

puppet-module-designate (17.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 15:55:16 +0200

puppet-module-designate (17.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Oct 2020 13:26:54 +0200

puppet-module-designate (16.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 11:33:40 +0200

puppet-module-designate (16.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 03 May 2020 12:56:55 +0200

puppet-module-designate (16.2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 26 Apr 2020 12:18:44 +0200

puppet-module-designate (15.4.0-3) unstable; urgency=medium

  * Depends on puppet, not puppet-common.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Mar 2020 09:57:14 +0100

puppet-module-designate (15.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Oct 2019 00:17:40 +0200

puppet-module-designate (15.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Oct 2019 10:51:53 +0200

puppet-module-designate (15.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed min version of runtime depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Sep 2019 11:26:35 +0200

puppet-module-designate (14.4.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * Switch build-dependencies to Python 3 (Closes: #937351).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 20:29:50 +0200

puppet-module-designate (14.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Jul 2019 21:50:38 +0200

puppet-module-designate (14.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Standards-Version: 4.3.0 (no change).

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Apr 2019 09:41:18 +0200

puppet-module-designate (13.1.0-1) unstable; urgency=medium

  * Initial release (Closes: #901683).

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Jun 2018 16:24:08 +0200
